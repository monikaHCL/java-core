package com.main;

import java.util.Scanner;

import com.modal.User;
import com.service.UserValidationBean;
import com.service.UserValidationRemote;

public class MainApplication {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		//Tale input from User First
		System.out.println("Enter the user id : ");
		int userId = scanner.nextInt();
		System.out.println("Enter the Password : ");
		String pwd = scanner.next();
		
		//create object of user class and initialize the user
		User user = new User(userId, pwd);
		
		//create object of service we want to use
		UserValidationRemote validateService = new UserValidationBean();
		
		/*//call method using only userID and pwd separate 
		String returnMessage = validateService.validateuserIdAndPassword(userId, pwd);
		if(returnMessage != null && !returnMessage.equals("")) {
			System.out.println("Welcome "+returnMessage);
		}*/
		
		//another service validate by user
		User validUser = validateService.validateUser(user);
		if(validUser != null) {
			System.out.println("Welcome "+validUser.getUserName());
			validUser = null;
		}else {
			System.err.println("Invalid Login !!");
		}
		
		validateService = null;
		scanner.close();
	}

}
