package com.service;

import com.modal.User;

public class UserValidationBean implements UserValidationRemote {

	@Override
	public String validateuserIdAndPassword(int userId, String pwd) {
		String message = "";
		//First Validation
		if(userId == 1234 && pwd.equals("hello")) {
			System.out.println("World");
		}else if(userId == 4567 && pwd.equals("break")) {
			message = "End of Training";
		}else {
			message = "Invalid User";
		}
		return message;

	}

	@Override
	public User validateUser(User user) {
		if(user.getUserId() == 1234 && user.getPwd().equals("hello")) {
			user.setUserName("User !!");
			return user;
		}else if(user.getUserId() == 4567 && user.getPwd().equals("break")) {
			user.setUserName("End Of training!!");
			return user;
		}
		return null;
	}

}
