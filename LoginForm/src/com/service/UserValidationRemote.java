package com.service;

import com.modal.User;

public interface UserValidationRemote {
	public abstract String validateuserIdAndPassword(int userId , String pwd);
	public abstract User validateUser(User user);

}
