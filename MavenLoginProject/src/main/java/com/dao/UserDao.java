package com.dao;
//Data Access Object
//Where all the database related activity

import com.model.User;

public interface UserDao {
	
	public abstract User createUser(User user);
	
	public abstract User readUser(int userId);
	
	public abstract User updateUser(String user);
	
	public abstract User deleteUser(int userId);
	
	public abstract User validateuserIdAndPassword(int userId , String pwd);

}
