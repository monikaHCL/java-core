package com.service;

import com.model.User;

public interface UserService {

	public abstract User createUser(User user);
	
	public abstract User readUser(int userId);
	
	public abstract User updateUser(String user);
	
	public abstract User deleteUser(int userId);
	
	public abstract User validateuserIdAndPassword(int userId , String pwd);
}
