package com.service;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.User;
//Maven + Login application --Validation userid > 5 and password > 5

public class UserServiceImpl implements UserService {

	public User createUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	public User readUser(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	public User updateUser(String user) {
		// TODO Auto-generated method stub
		return null;
	}

	public User deleteUser(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	public User validateuserIdAndPassword(int userId, String pwd) {
		User user = null;
		//This Layer is used to business validation
		
		int idLength = String.valueOf(userId).length();
		System.out.println(idLength);
		if(idLength >= 5 && pwd.length() > 5) {
			UserDao userDao = new UserDaoImpl();
			user = userDao.validateuserIdAndPassword(userId, pwd);
		}
		return user;
	}

}
